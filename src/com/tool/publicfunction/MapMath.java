package com.tool.publicfunction;

import android.graphics.Bitmap;
import android.view.View;
import android.view.View.MeasureSpec;

//public class MapMath
//{
//	/************************** 计算两点间距离的常量 ****************************/
//	private static double DEF_PI = 3.14159265359; // PI
//	private static double DEF_2PI = 6.28318530712; // 2*PI
//	private static double DEF_PI180 = 0.01745329252; // PI/180.0
//	private static double DEF_R = 6370693.5; // radius of earth
//
//
//	/**** 百度官方的计算两点间距离的方法******短距离采用勾股定理****长距离采用按标准的球面大圆劣弧长度计算 ****/
//	public double GetShortDistance(double lon1, double lat1, double lon2,
//			double lat2)
//	{
//		double ew1, ns1, ew2, ns2;
//		double dx, dy, dew;
//		double distance;
//		// 角度转换为弧度
//		ew1 = lon1 * DEF_PI180;
//		ns1 = lat1 * DEF_PI180;
//		ew2 = lon2 * DEF_PI180;
//		ns2 = lat2 * DEF_PI180;
//		// 经度差
//		dew = ew1 - ew2;
//		// 若跨东经和西经180 度，进行调整
//		if (dew > DEF_PI)
//			dew = DEF_2PI - dew;
//		else if (dew < -DEF_PI)
//			dew = DEF_2PI + dew;
//		dx = DEF_R * Math.cos(ns1) * dew; // 东西方向长度(在纬度圈上的投影长度)
//		dy = DEF_R * (ns1 - ns2); // 南北方向长度(在经度圈上的投影长度)
//		// 勾股定理求斜边长
//		distance = Math.sqrt(dx * dx + dy * dy);
//		return distance;
//	}
//
//	// 经纬度转换为墨卡托坐标
//	public double[] latlog2Mkt(Double lat, Double log)
//	{
//
//		double[] xy = new double[2];
//		double x = log * 20037508.342789 / 180;
//		double y = Math.log(Math.tan((90 + lat) * Math.PI / 360))
//				/ (Math.PI / 180);
//		y = y * 20037508.34789 / 180;
//		xy[0] = x;
//		xy[1] = y;
//		return xy;
//
//	}
//
//	// 计算两点间距离
//	public int CalculateDistance(double[] xy1, double[] xy2)
//	{
//		double x1 = xy1[0];
//		double y1 = xy1[1];
//		double x2 = xy2[0];
//		double y2 = xy2[1];
//		double delta_x = x1 - x2;
//		double delta_y = y1 - y2;
//		int Distance = (int) (Math.sqrt(delta_x * delta_x + delta_y * delta_y));
//		return Distance;
//
//	}
//
//	/***************** 采用冒泡排序法得到最短距离 **************/
//	public double sort(Double[] distance_list)
//	{
//		for (int i = 1; i < distance_list.length; i++)
//		{
//			for (int j = 0; j < distance_list.length - i; j++)
//			{
//				if (distance_list[j] > distance_list[j + 1])
//				{
//					double temp = distance_list[j];
//					distance_list[j] = distance_list[j + 1];
//					distance_list[j + 1] = temp;
//				}
//			}
//		}
//		/******** 排序后的距离 *******/
//		double min = distance_list[0];
//		return min;
//	}
//	
//	
//	/***将View转换为BitMap对象****/
//	public static Bitmap convertViewToBitmap(View view)
//	{
//		view.measure(
//				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED),
//				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
//		view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
//		view.buildDrawingCache();
//		Bitmap bitmap = view.getDrawingCache();
//
//		return bitmap;
//	}
//
//}
