package com.tool.subway;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Parcelable;
import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

public class UsehelpActivity extends Activity
{

	private ViewPager viewPager; // 实现滑动效果的View
	private ArrayList<View> pageViews;
	private ImageView imageView;
	private ImageView[] imageViews;
	// 包裹滑动图片Linearlayout
	private ViewGroup Viewmain;
	// 包裹小圆点的Linearlayout
	private ViewGroup group;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);// 无标题栏
		// 加载布局文件
		LayoutInflater inflater = getLayoutInflater();
		// 定义一个View型的动态数组，添加功能引导界面
		pageViews = new ArrayList<View>();
		pageViews.add(inflater.inflate(R.layout.use_guide01, null));
		pageViews.add(inflater.inflate(R.layout.use_guide02, null));
		pageViews.add(inflater.inflate(R.layout.use_guide03, null));
		pageViews.add(inflater.inflate(R.layout.use_guide04, null));
		pageViews.add(inflater.inflate(R.layout.use_guide05, null));
		pageViews.add(inflater.inflate(R.layout.use_guide06, null));
		pageViews.add(inflater.inflate(R.layout.use_guide07, null));
		pageViews.add(inflater.inflate(R.layout.use_guide08, null));
		pageViews.add(inflater.inflate(R.layout.use_guide09, null));
		pageViews.add(inflater.inflate(R.layout.use_guide10, null));

		// 实例化数组
		imageViews = new ImageView[pageViews.size()];
		// 包裹图片的布局
		Viewmain = (ViewGroup) inflater
				.inflate(R.layout.activity_usehelp, null);
		// 包裹小圆点的布局
		group = (ViewGroup) Viewmain.findViewById(R.id.viewGroup);
		// 实现滑动效果的View
		viewPager = (ViewPager) Viewmain.findViewById(R.id.guidePages);

		for (int i = 0; i < pageViews.size(); i++)
		{
			imageView = new ImageView(UsehelpActivity.this);
			imageView.setLayoutParams(new LayoutParams(20, 20));
			imageView.setPadding(0, 0, 0, 0);
			imageViews[i] = imageView;

			if (i == 0)
			{
				// 默认选中第一张图片
				imageViews[i]
						.setBackgroundResource(R.drawable.page_indicator_focused);
			}
			else
			{
				imageViews[i].setBackgroundResource(R.drawable.page_indicator);
			}

			group.addView(imageViews[i]);
		}
		// 设置显示的View
		setContentView(Viewmain);

		viewPager.setAdapter(new MyguideAdapter());
		viewPager.setOnPageChangeListener(new MyGuidePageChangeListener());
	}

	/***************** 重写PagerView的适配器 指引页面数据适配器 ****************/
	public class MyguideAdapter extends PagerAdapter
	{
		/********** 返回数组的大小 ***************************/
		@Override
		public int getCount()
		{
			// TODO Auto-generated method stub
			return pageViews.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1)
		{
			// TODO Auto-generated method stub
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object)
		{
			// TODO Auto-generated method stub
			((ViewPager) container).removeView(pageViews.get(position));
		}

		@Override
		public void finishUpdate(ViewGroup container)
		{
			// TODO Auto-generated method stub
			super.finishUpdate(container);
		}

		/************************** 获取当前视图对象的位置 **********************************/
		@Override
		public int getItemPosition(Object object)
		{
			return super.getItemPosition(object);
		}

		@Override
		public CharSequence getPageTitle(int position)
		{
			return super.getPageTitle(position);
		}

		@Override
		public float getPageWidth(int position)
		{
			return super.getPageWidth(position);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position)
		{
			((ViewPager) container).addView(pageViews.get(position));
			return pageViews.get(position);
		}

		@Override
		public void notifyDataSetChanged()
		{
			super.notifyDataSetChanged();
		}

		@Override
		public void restoreState(Parcelable state, ClassLoader loader)
		{
			super.restoreState(state, loader);
		}

		@Override
		public Parcelable saveState()
		{
			return super.saveState();
		}

		@Override
		public void setPrimaryItem(ViewGroup container, int position,
				Object object)
		{
			super.setPrimaryItem(container, position, object);
		}

		@Override
		public void startUpdate(ViewGroup container)
		{
			super.startUpdate(container);
		}

	}

	/******************** 指引页面更改事件监听器 ***********************/
	public class MyGuidePageChangeListener implements OnPageChangeListener
	{

		@Override
		public void onPageScrollStateChanged(int arg0)
		{

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2)
		{

		}

		@Override
		public void onPageSelected(int arg0)
		{

			for (int i = 0; i < imageViews.length; i++)
			{
				// 当选中当前图片时，设置为选中的圆形
				imageViews[arg0]
						.setBackgroundResource(R.drawable.page_indicator_focused);

				if (arg0 != i)
				{
					imageViews[i]
							.setBackgroundResource(R.drawable.page_indicator);
				}
			}

		}
	}
}
