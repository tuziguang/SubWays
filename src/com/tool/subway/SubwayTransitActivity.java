package com.tool.subway;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class SubwayTransitActivity extends Activity
{
	private WebView webview;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//必须在加载布局之前
		requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_subway_transit);


		webview = (WebView) findViewById(R.id.Webview_ditiegongjiao);
		if (webview != null)
		{
			webview.setWebViewClient(new WebViewClient()
			{

				@Override
				public void onPageFinished(WebView view, String url)
				{
					// TODO Auto-generated method stub
					dialog.dismiss();
					super.onPageFinished(view, url);
				}
				
			});
			//8684公交网址
			String path="http://m.8684.cn/wuhan_bus";
			loadurl(path);
		}
		//使用内置的缩放控件
		webview.getSettings().setSupportZoom(true);
		webview.getSettings().setBuiltInZoomControls(true);
	}

	private void loadurl(String url)
	{
		if (webview!=null)
		{
			webview.loadUrl(url);
			dialog=ProgressDialog.show(this, null, "页面加载中，请稍后..");
			webview.reload();
		}
	}

}
