package com.tool.subway;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class CDhansubwayGuanwangActivity extends Activity
{
	private WebView wuhansubwayWeb;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_PROGRESS);// 让进度条显示在标题栏
		setContentView(R.layout.activity_wuhansubway_guanwang);
		wuhansubwayWeb = (WebView) findViewById(R.id.wuhansubwayWeb);

		if (wuhansubwayWeb != null)
		{
			wuhansubwayWeb.setWebViewClient(new WebViewClient()
			{
				@Override
				public void onPageFinished(WebView view, String url)
				{
					// TODO Auto-generated method stub
					dialog.dismiss();
					super.onPageFinished(view, url);
				}
			});
			// 加载成都地铁官方网站
			String path = "http://www.cdmetro.cn/";
			loadWeb(path);

		}

		// 放大缩小的实现,手势多点触控
		wuhansubwayWeb.getSettings().setSupportZoom(true);
		wuhansubwayWeb.getSettings().setBuiltInZoomControls(true);
	}

	public void loadWeb(String url)
	{
		if (url!=null)
		{
			wuhansubwayWeb.loadUrl(url);
			dialog=ProgressDialog.show(CDhansubwayGuanwangActivity.this, null, "页面加载中，请稍后..");
		}
	}
}
