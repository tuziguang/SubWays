package com.tool.subway;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class LostfoundActivity extends Activity
{
	private WebView webviewLostfound;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_lostfound);

		webviewLostfound = (WebView) findViewById(R.id.Webview_LostFound);
		if (webviewLostfound != null)
		{
			webviewLostfound.setWebViewClient(new WebViewClient()
			{
				@Override
				// 重写加载完页面事件
				public void onPageFinished(WebView view, String url)
				{
					// TODO Auto-generated method stub
					dialog.dismiss();
					super.onPageFinished(view, url);
				}
			});
			// 加载地址
			String path = "http://www.scqcp.com/";
			loadurl(path);
		}
		//设置放大缩小
		webviewLostfound.getSettings().setSupportZoom(true);
		webviewLostfound.getSettings().setBuiltInZoomControls(true);
	}

	public void loadurl(String url)
	{
		if (webviewLostfound!=null)
		{
			webviewLostfound.loadUrl(url);
			dialog=ProgressDialog.show(LostfoundActivity.this, null, "页面加载中，请稍后……");
			webviewLostfound.reload();
		}
	}
}
