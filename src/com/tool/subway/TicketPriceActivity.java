package com.tool.subway;

import java.util.ArrayList;
import java.util.Arrays;

import com.tool.view.wheel.widget.ArrayWheelAdapter;
import com.tool.view.wheel.widget.OnWheelChangedListener;
import com.tool.view.wheel.widget.WheelView;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class TicketPriceActivity extends Activity
{
	private Button btnStart;
	private Button btnEnd;
	private Button btnQuery;
	// 定义标识当前线路和站点的变量
	private int CurrentLine;
	private int CurrentStation;
	private String from = "";
	private String to = "";
	// 地铁线路
	private static final String[] LineId = new String[] { "1 号 线", "2 号 线"

	};
	// // 滚轮站点
	final String stations[][] = new String[][] {
			new String[] { "堤角", "新荣", "丹水池", "徐州新村", "二七路", "头道街", "黄浦路",
					"三阳路", "大智路", "循礼门", "友谊路", "利济北路", "崇仁路", "硚口", "太平洋",
					"宗关", "汉西一路", "古田四路", "古田三路", "古田二路", "古田一路", "舵落口", "额头湾",
					"五环大道", "东吴大道" },
			new String[] {

			"金银潭", "常青花园", "长港路", "汉口火车站", "范湖", "王家墩东", "青年路", "中山公园", "循礼门",
					"江汉路", "积玉桥", "螃蟹岬", "小龟山", "洪山广场", "中南路", "宝通寺", "街道口",
					"广埠屯", "虎泉", "杨家湾", "光谷广场" }, };
	// 票价
	protected int price;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ticket_price);

		btnStart = (Button) findViewById(R.id.btnSelectStart);
		btnEnd = (Button) findViewById(R.id.btnSelectEnd);
		btnQuery = (Button) findViewById(R.id.btnQuery);

		/*选择起点*/
		btnStart.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v )
			{
				final AlertDialog chooseStationDialog = new AlertDialog.Builder(
						TicketPriceActivity.this).create();
				LayoutInflater factory = LayoutInflater
						.from(TicketPriceActivity.this);
				final View ChooseStationView = factory.inflate(
						R.layout.selectstation, null);
				chooseStationDialog.setView(ChooseStationView);
				chooseStationDialog.setTitle("请选择站点");
				chooseStationDialog.show();
				// 实例化控件
				final WheelView WheelLine = (WheelView) ChooseStationView
						.findViewById(R.id.wheelLine);
				final WheelView WheelStation = (WheelView) ChooseStationView
						.findViewById(R.id.wheelStation);
				final Button BtnOK = (Button) ChooseStationView
						.findViewById(R.id.btn_ok);
				final Button BtnCancle = (Button) ChooseStationView
						.findViewById(R.id.btn_cancle);
				// 填充数据
				WheelLine.setAdapter(new ArrayWheelAdapter<String>(LineId));
				WheelLine.setVisibleItems(3); // 设置当前可见数线路可见为3
				WheelStation.setVisibleItems(5);// 站点可见为5
				// 为线路滚动添加监听
				WheelLine.addChangingListener(new OnWheelChangedListener()
				{

					@Override
					public void onChanged(WheelView wheel, int oldValue,
							int newValue)
					{
						WheelStation.setAdapter(new ArrayWheelAdapter<String>(
								stations[newValue]));
						WheelStation.setCurrentItem(0);
						CurrentLine = WheelLine.getCurrentItem(); // 获取当前线路ID
					}
				});
				// 当前默认设为2号线
				WheelLine.setCurrentItem(1);

				// 为站点滚动添加监听
				WheelStation.addChangingListener(new OnWheelChangedListener()
				{

					@Override
					public void onChanged(WheelView wheel, int oldValue,
							int newValue)
					{

						// 获取当前的站点索引号
						CurrentStation = WheelStation.getCurrentItem();

					}
				});
				// 确定按钮监听
				BtnOK.setOnClickListener(new View.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						final ArrayList<String> ArrayCurrentStation = new ArrayList<String>();
						ArrayCurrentStation.addAll(Arrays
								.asList(stations[CurrentLine]));
						// 获取当前站点
						String stringCurrentStation = ArrayCurrentStation.get(
								CurrentStation).toString();

						btnStart.setText(stringCurrentStation);
						chooseStationDialog.dismiss();
						// 起点
						from = stringCurrentStation;
					}
				});
				// 取消按钮监听
				BtnCancle.setOnClickListener(new View.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						// TODO Auto-generated method stub
						chooseStationDialog.dismiss();
					}
				});

			}
		});
		// 选择终点
		btnEnd.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				// 创建一个对话框
				final AlertDialog chooseStationDialog = new AlertDialog.Builder(
						TicketPriceActivity.this).create();
				LayoutInflater factory = LayoutInflater
						.from(TicketPriceActivity.this);
				final View ChooseStationView = factory.inflate(
						R.layout.selectstation, null);
				// 设置其显示的View
				chooseStationDialog.setView(ChooseStationView);

				chooseStationDialog.setTitle("请选择站点");
				// 获取当前对话框的属性，并设置其宽和高去除边框
				// 去除对话框的边框
				// chooseStationDialog.setView(ChooseStationView, 0, 0, 0, 0);
				// WindowManager.LayoutParams params = chooseStationDialog
				// .getWindow().getAttributes();
				// params.width = 320;
				// params.height = 300;
				// chooseStationDialog.getWindow().setAttributes(params);
				chooseStationDialog.show();
				// 设置对话框的样式，美化
				// 实例化控件
				final WheelView WheelLine = (WheelView) ChooseStationView
						.findViewById(R.id.wheelLine);
				final WheelView WheelStation = (WheelView) ChooseStationView
						.findViewById(R.id.wheelStation);
				final Button BtnOK = (Button) ChooseStationView
						.findViewById(R.id.btn_ok);
				final Button BtnCancle = (Button) ChooseStationView
						.findViewById(R.id.btn_cancle);
				// 填充数据
				WheelLine.setAdapter(new ArrayWheelAdapter<String>(LineId));
				WheelLine.setVisibleItems(3); // 设置当前可见数线路可见为3
				WheelStation.setVisibleItems(5);// 站点可见为5
				// 为线路滚动添加监听
				WheelLine.addChangingListener(new OnWheelChangedListener()
				{

					@Override
					public void onChanged(WheelView wheel, int oldValue,
							int newValue)
					{
						WheelStation.setAdapter(new ArrayWheelAdapter<String>(
								stations[newValue]));
						WheelStation.setCurrentItem(0);
						CurrentLine = WheelLine.getCurrentItem(); // 获取当前线路ID
					}
				});
				// 当前默认设为2号线
				WheelLine.setCurrentItem(1);
				// 为站点滚动添加监听
				WheelStation.addChangingListener(new OnWheelChangedListener()
				{

					@Override
					public void onChanged(WheelView wheel, int oldValue,
							int newValue)
					{

						// 获取当前的站点索引号
						CurrentStation = WheelStation.getCurrentItem();

					}
				});
				// 确定按钮监听
				BtnOK.setOnClickListener(new View.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						final ArrayList<String> ArrayCurrentStation = new ArrayList<String>();
						ArrayCurrentStation.addAll(Arrays
								.asList(stations[CurrentLine]));
						// 获取当前站点
						String stringCurrentStation = ArrayCurrentStation.get(
								CurrentStation).toString();

						btnEnd.setText(stringCurrentStation);
						chooseStationDialog.dismiss();
						// 终点
						to = stringCurrentStation;
					}
				});
				// 取消按钮监听
				BtnCancle.setOnClickListener(new View.OnClickListener()
				{

					@Override
					public void onClick(View v)
					{
						// TODO Auto-generated method stub
						chooseStationDialog.dismiss();
					}
				});
			}

		});
		// 查询票价
		btnQuery.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				if ("".equals(from))
				{
					Toast.makeText(getApplicationContext(), "请选择起点", 1)
							.show();
				}
				else if ("".equals(to))
				{
					Toast.makeText(TicketPriceActivity.this, "请选择终点", 1).show();
				}
				else
				{
					Ticketprice ticketprice = new Ticketprice(from, to);
					price = ticketprice.QueryPrice(from, to);
					Toast toast;
					String strprice = "票价为：" + String.valueOf(price) + "元";
					toast = Toast
							.makeText(getApplicationContext(), strprice, 1);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

				}
			}
		});
	}
}
