package com.tool.subway;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EdgeEffect;
import android.widget.ListView;

public class CDmetroStationAdapter extends Activity {
	private ListView listViewline;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.metro_station_adapter);
		Log.v("onCreate", "SubwayoneActivity");

		// 分支判定
		// 上一级Activity
		Intent intent = getIntent();
		final String functionString = intent.getStringExtra("functionString");

		// 按钮刷新ListView
		findViewById(R.id.btn_adapter_line1).setOnClickListener(
				new OnClickListener() {
					// 线路1站点
					private String[] stringsline = new String[] { "升仙湖", "火车北",
							"人民北路", "文殊院", "骡马市", "天府广场", "锦江宾馆", "华西坝",
							"省体育馆", "倪家桥", "桐梓林", "火车南", "高新", "金融城", "孵化园",
							"锦城广场", "世纪城", "天府三街", "天府五街", "华府大道", "四河", "广都" };

					@Override
					public void onClick(View v) {
						onClickStation(stringsline, functionString);
					}
				});

		findViewById(R.id.btn_adapter_line2).setOnClickListener(
				new OnClickListener() {
					// 线路2点
					private String[] stringsline = new String[] { "龙泉驿", "龙平路",
							"书房", "界牌", "连山坡", "大面铺", "成都行政学院", "洪河", "惠王陵",
							"成渝立交", "成都东客", "塔子山公园", "东大路", "牛市口", "牛王庙",
							"东门大桥", "春熙路", "天府广场", "人民公园", "通惠门", "中医大省医院",
							"白果林", "蜀汉路东", "一品天下", "羊犀立交", "茶店子客运", "迎宾大道",
							"金科北路", "金周路", "百草路", "天河路", "犀浦" };

					@Override
					public void onClick(View v) {
						onClickStation(stringsline, functionString);
					}
				});
	}

	private void onClickStation(final String[] stringsline,
			final String functionString) {
		// 加载线路适配器
		listViewline = (ListView) findViewById(R.id.listview_metro_item);
		listViewline.setAdapter(new ArrayAdapter<String>(
				CDmetroStationAdapter.this, R.layout.list_item_0, stringsline));
		// 点击事件监听
		listViewline.setOnItemClickListener(new OnItemClickListener() {

			@Override
			// 第三个参数是默认的索引位置
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				SharedPreferences stationPreferences = getSharedPreferences(
						"info", MODE_PRIVATE);
				Editor stationEditor = stationPreferences.edit();
				switch (functionString) {
				case "CDmetroSearch":
					Intent intent1 = new Intent(CDmetroStationAdapter.this,
							CDmetroSearch.class);
					intent1.putExtra("List_ZD", stringsline[position]);
					startActivity(intent1);
					break;
				case "CDmetroRoutePlan_start":
					Intent intent2 = new Intent(CDmetroStationAdapter.this,
							CDmetroRoutePlan.class);
					// intent2.putExtra("List_ZD", stringsline[position]);
					stationEditor.putString("stationStart",
							stringsline[position]);
					stationEditor.commit();
					startActivity(intent2);
					break;
				case "CDmetroRoutePlan_end":
					Intent intent3 = new Intent(CDmetroStationAdapter.this,
							CDmetroRoutePlan.class);
					// intent3.putExtra("List_ZD", stringsline[position]);
					stationEditor
							.putString("stationEnd", stringsline[position]);
					stationEditor.commit();
					startActivity(intent3);
					break;
				default:
					Intent intent4 = new Intent(CDmetroStationAdapter.this,
							CDmetroSearch.class);
					intent4.putExtra("List_ZD", stringsline[position]);
					startActivity(intent4);
					break;
				}

			}
		});
	}

}
