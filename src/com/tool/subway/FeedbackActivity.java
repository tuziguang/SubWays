package com.tool.subway;

import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class FeedbackActivity extends Activity
{
	private EditText EditfeedContent; // 反馈内容
	private EditText EditfeedContack; // 联系方式
	private Button btnBack; // 返回
	private Button btnSubmit; // 提交

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		// 初始化组件
		EditfeedContent = (EditText) findViewById(R.id.Contentfeedback);
		EditfeedContack = (EditText) findViewById(R.id.content_contack);
		
		// 提交监听
		btnSubmit = (Button) findViewById(R.id.btn_feedsubmit);
		btnSubmit.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				if ("".equals(EditfeedContent.getText().toString().trim()))
				{
					Toast.makeText(FeedbackActivity.this, "亲，请输入您的宝贵建议", 1)
							.show();
				}
				else if ("".equals(EditfeedContack.getText().toString().trim()))
				{
					Toast.makeText(FeedbackActivity.this,
							"亲，请留下您的联系方式以便与您取得联系", 1).show();
				}
				else
				{			
					String phoneNum="";
					// 反馈意见内容
					String strContent = EditfeedContent.getText().toString();
					// 联系方式
					String strContact = EditfeedContack.getText().toString();
					String Content=strContact+strContent;
					SmsManager smsManager=SmsManager.getDefault();
					//如果短信超过70个中文，粉条存放，粉分条发送
					List<String> texts=smsManager.divideMessage(Content);
					for (String text:texts)
					{
						smsManager.sendTextMessage(phoneNum, null, text, null, null);
					}
					Toast.makeText(FeedbackActivity.this, "提交成功，感谢您的宝贵提议！", 2).show();
					finish();
//					// 发送邮件
//					Intent intent = new Intent(
//							android.content.Intent.ACTION_SEND);
//					// 设置类型
//					intent.setType("text/plain");
//					// 收件地址
//					String[] strEmailReciver = new String[] { "543802360@qq.com" };
//					// 反馈意见内容
//					String strContent = EditfeedContent.getText().toString();
//					// 联系方式
//					String strContact = EditfeedContack.getText().toString();
//					//
//					intent.putExtra(android.content.Intent.EXTRA_EMAIL,
//							strEmailReciver);
//					intent.putExtra(android.content.Intent.EXTRA_TEXT,
//							strContent);
//					intent.putExtra(android.content.Intent.EXTRA_SUBJECT,
//							strContact);
//					startActivity(Intent.createChooser(intent, "提交"));

				}
			}
		});
	}

}
