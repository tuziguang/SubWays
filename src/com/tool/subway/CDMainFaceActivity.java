package com.tool.subway;

import android.R.color;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class CDMainFaceActivity extends Activity {

	TextView mTextSubwayXLT;
	TextView mTextSubwayRouteQuery;
	TextView mTextSubwayStation;
	TextView mTextSubwayNearByStation;
	TextView mTextSubwayMylocation;
	TextView mTextSubwayUserGuide;
	TextView mTextSubwayMore;
	TextView mTextSubwayRoute;
	TextView mTextSubwayMetroSearch;
	TextView mTextSubwayMetroNearByStation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_face);
		mTextSubwayMore = (TextView) findViewById(R.id.mText_whsubway_more);
		mTextSubwayXLT = (TextView) findViewById(R.id.mText_whsubway_XLT);
		mTextSubwayRoute = (TextView) findViewById(R.id.mText_whsubway_route1);
		mTextSubwayMetroSearch = (TextView) findViewById(R.id.mText_whsubway_station1);
		mTextSubwayMetroNearByStation = (TextView) findViewById(R.id.mText_whsubway_nearByStation1);

		OnClickListener onClickListener = new OnClickListener() {

			@Override
			public void onClick(View v) {
				SearchViewPressed(v);
			}
		};

		mTextSubwayMore.setOnClickListener(onClickListener);
		mTextSubwayXLT.setOnClickListener(onClickListener);
		mTextSubwayRoute.setOnClickListener(onClickListener);
		mTextSubwayMetroSearch.setOnClickListener(onClickListener);
		mTextSubwayMetroNearByStation.setOnClickListener(onClickListener);
	}

	public void SearchViewPressed(View view) {
		setBackgroundColor();

		if (mTextSubwayMore.equals(view)) {
			mTextSubwayMore.setBackgroundResource(R.color.gray);
			Intent intent = new Intent(getApplicationContext(),
					MoreActivity.class);
			startActivity(intent);
		} else if (mTextSubwayXLT.equals(view)) {
			mTextSubwayXLT.setBackgroundResource(R.color.gray);
			Intent intent = new Intent(getApplicationContext(),
					CDmetroMap.class);
			startActivity(intent);
		} else if (mTextSubwayRoute.equals(view)) {
			mTextSubwayRoute.setBackgroundResource(R.color.gray);
			Intent intent = new Intent(CDMainFaceActivity.this,
					CDmetroRoutePlan.class);
			startActivity(intent);
		} else if (mTextSubwayMetroSearch.equals(view)) {
			mTextSubwayMetroSearch.setBackgroundResource(R.color.gray);
			Intent intent = new Intent(CDMainFaceActivity.this,
					CDmetroStationAdapter.class);
			intent.putExtra("functionString", "CDmetroSearch");
			startActivity(intent);
		} else if (mTextSubwayMetroNearByStation.equals(view)) {
			mTextSubwayMetroNearByStation
					.setBackgroundResource(R.color.gray);
			Intent intent = new Intent(CDMainFaceActivity.this,
					CDPoiSearchNearBy.class);
			startActivity(intent);
		}
	}

	/****** 设置全部背景均为透明色 ****/
	public void setBackgroundColor() {
		mTextSubwayMore.setBackgroundResource(color.transparent);
		mTextSubwayXLT.setBackgroundResource(color.transparent);
		mTextSubwayRoute.setBackgroundResource(color.transparent);
		mTextSubwayMetroSearch.setBackgroundResource(color.transparent);
		mTextSubwayMetroNearByStation.setBackgroundResource(color.transparent);

	}

	/**************** 退出程序 ********************/
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			/****** 弹出插播广告 ***********/

			new AlertDialog.Builder(this)
					.setMessage("确定要退出成都地铁助手吗？")
					.setNegativeButton("取消",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub

								}
							})
					.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									finish();
								}
							}).show();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

}
