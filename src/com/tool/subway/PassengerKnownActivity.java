package com.tool.subway;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PassengerKnownActivity extends Activity
{

	private WebView webviewPassenger;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_passenger_known);

		webviewPassenger = (WebView) findViewById(R.id.Webview_passengerknown);

		if (webviewPassenger != null)
		{
			webviewPassenger.setWebViewClient(new WebViewClient()
			{

				@Override
				public void onPageFinished(WebView view, String url)
				{
					// TODO Auto-generated method stub
					dialog.dismiss();
				}

			});
			// 加载成都地铁官方网站 乘客须知
			String path = "http://www.scqcp.com/";
			loadWeb(path);

		}
		// 放大缩小的实现,手势多点触控
		webviewPassenger.getSettings().setSupportZoom(true);
		webviewPassenger.getSettings().setBuiltInZoomControls(true);
	}

	public void loadWeb(String url)
	{
		if (webviewPassenger != null)
		{
			webviewPassenger.loadUrl(url);
			dialog = ProgressDialog.show(this, null, "页面加载中，请稍后..");
			webviewPassenger.reload();
		}

	}

}
