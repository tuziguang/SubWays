package com.tool.subway;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MoreActivity extends Activity
{
	// 全局变量
	private RelativeLayout layout_passengerKnow; // 乘客须知
	private RelativeLayout layout_WuhanditiePhone; // 成都地铁热线
	private RelativeLayout layout_WuhanditieGuanwang; // 成都地铁官方网站
	private RelativeLayout layout_mianzeshengming; // 免责声明
	private RelativeLayout layout_feedback; // 反馈
	private RelativeLayout layout_jianchaxinbanben; // 检查更新
	private RelativeLayout layout_help; // 使用指南
	private RelativeLayout layout_about; // 关于
	private RelativeLayout layout_setting; // 设置
	private RelativeLayout layout_piaowuguiding;// 票务规定
	private RelativeLayout layout_ticket_price; // 票价查询
	private RelativeLayout layout_ditiegongjiao;// 地铁公交
	private RelativeLayout layout_lostfound; // 失物招领

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_more);
		// 乘客须知
		layout_passengerKnow = (RelativeLayout) findViewById(R.id.layout_passengerKnow);
		layout_passengerKnow.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// TODO Auto-generated method stub
				// 设置选中背景颜色

				layout_passengerKnow
						.setBackgroundResource(R.drawable.more_pressed);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);

				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				Intent intent = new Intent(MoreActivity.this,
						PassengerKnownActivity.class);
				startActivity(intent);
			}
		});
		// 成都地铁票务规定
		layout_piaowuguiding = (RelativeLayout) findViewById(R.id.layout_piaowuguiding);
		layout_piaowuguiding.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				// TODO Auto-generated method stub
				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.more_pressed);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				Intent intent = new Intent(MoreActivity.this,
						PiaowuguidingActivity.class);
				startActivity(intent);

			}
		});
		// 成都地铁热线
		layout_WuhanditiePhone = (RelativeLayout) findViewById(R.id.layout_WuhanditiePhone);
		layout_WuhanditiePhone.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.more_pressed);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				final AlertDialog phoneDialog = new AlertDialog.Builder(
						MoreActivity.this).create();
				phoneDialog.setTitle("提示");
				phoneDialog.setMessage("您确定要拨打成都地铁服务热线027—83749240吗？");
				phoneDialog.setButton("确定",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
								String phoneNum = "027-83749240";
								Intent intent = new Intent(Intent.ACTION_CALL,
										Uri.parse("tel:" + phoneNum));
								startActivity(intent);

							}
						});
				phoneDialog.setButton2("取消",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
								// TODO Auto-generated method stub
								phoneDialog.dismiss();
							}
						});
				phoneDialog.show();

			}
		});
		// 成都地铁官方网站
		layout_WuhanditieGuanwang = (RelativeLayout) findViewById(R.id.layout_WuhanditieGuanwang);
		layout_WuhanditieGuanwang.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.more_pressed);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				Intent intent = new Intent(MoreActivity.this,
						CDhansubwayGuanwangActivity.class);
				startActivity(intent);

			}
		});
		// 免责声明
		layout_mianzeshengming = (RelativeLayout) findViewById(R.id.layout_mianzeshengming);
		layout_mianzeshengming.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.more_pressed);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				// 跳转
				// Intent intent = new Intent(MoreActivity.this,
				// MianzeshengmingActivity.class);
				// startActivity(intent);

				final AlertDialog AboutmeDialog = new AlertDialog.Builder(
						MoreActivity.this).create();
				AboutmeDialog.setTitle("免责声明");
				AboutmeDialog
						.setMessage("作者致力于构建数字城市、智慧城市，开发了这套软件。但本软件所提供的资料内容不应作为任何决定或行动的基准，由此引发的任何纠纷，作者不承担任何法律责任");
				AboutmeDialog.setButton("确定",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
								// TODO Auto-generated method stub
								AboutmeDialog.dismiss();
							}
						});
				AboutmeDialog.setButton2("取消",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
								// TODO Auto-generated method stub
								AboutmeDialog.dismiss();
							}
						});
				AboutmeDialog.show();

			}
		});
		// 意见反馈
		layout_feedback = (RelativeLayout) findViewById(R.id.layout_feedback);
		layout_feedback.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// 设置选中背景颜色
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.more_pressed);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				Intent intent = new Intent(MoreActivity.this,
						FeedbackActivity.class);
				startActivity(intent);

			}
		});
		// 检查更新
		layout_jianchaxinbanben = (RelativeLayout) findViewById(R.id.layout_jianchaxinbanben);
		layout_jianchaxinbanben.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);

				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.more_pressed);
				Toast.makeText(MoreActivity.this, "当前已是最新版本！", 1).show();
			}
		});
		// 使用帮助
		layout_help = (RelativeLayout) findViewById(R.id.layout_help);
		layout_help.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.more_pressed);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				// 跳转
				Intent intent = new Intent(MoreActivity.this,
						UsehelpActivity.class);
				startActivity(intent);

			}
		});
		// 关于我
		layout_about = (RelativeLayout) findViewById(R.id.layout_aboutme);
		layout_about.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.more_pressed);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				layout_about.setBackgroundResource(R.drawable.more_pressed);
				final AlertDialog AboutmeDialog = new AlertDialog.Builder(
						MoreActivity.this).create();
				AboutmeDialog.setTitle("关于我");
				AboutmeDialog
						.setMessage("成都地铁\n作者：行云流水\n地址：成都市洪山区\n联系方式：···············@qq.com\n");
				AboutmeDialog.setButton("确定",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
								// TODO Auto-generated method stub
								AboutmeDialog.dismiss();
							}
						});
				AboutmeDialog.setButton2("取消",
						new DialogInterface.OnClickListener()
						{

							@Override
							public void onClick(DialogInterface dialog,
									int which)
							{
								// TODO Auto-generated method stub
								AboutmeDialog.dismiss();
							}
						});
				AboutmeDialog.show();
			}
		});
		// 设置
		layout_setting = (RelativeLayout) findViewById(R.id.layout_setting);
		layout_setting.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.more_pressed);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				// 跳转至系统的设置
				startActivity(new Intent(
						android.provider.Settings.ACTION_WIRELESS_SETTINGS));

			}
		});
		// 地铁公交
		layout_ditiegongjiao = (RelativeLayout) findViewById(R.id.layout_ditiegongjiao);
		layout_ditiegongjiao.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.more_pressed);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

				Intent intent = new Intent(MoreActivity.this,
						SubwayTransitActivity.class);
				startActivity(intent);
			}
		});
		// 票价查询
		layout_ticket_price = (RelativeLayout) findViewById(R.id.layout_piaojiachaxun);
		layout_ticket_price.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.more_pressed);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);

				layout_lostfound.setBackgroundResource(R.drawable.list_mid_nor);

//				Intent intent = new Intent(MoreActivity.this,
//						TicketPriceActivity.class);
//				startActivity(intent);
			}
		});
		// 失物招领
		layout_lostfound = (RelativeLayout) findViewById(R.id.layout_lostfound);
		layout_lostfound.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// 设置选中背景颜色
				layout_ticket_price
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_ditiegongjiao
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_piaowuguiding
						.setBackgroundResource(R.drawable.list_mid_nor);

				layout_passengerKnow
						.setBackgroundResource(R.drawable.list_above_nor);
				layout_WuhanditiePhone
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_WuhanditieGuanwang
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_mianzeshengming
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_feedback.setBackgroundResource(R.drawable.list_mid_nor);
				layout_jianchaxinbanben
						.setBackgroundResource(R.drawable.list_mid_nor);
				layout_help.setBackgroundResource(R.drawable.list_mid_nor);
				layout_about.setBackgroundResource(R.drawable.list_mid_nor);
				layout_setting.setBackgroundResource(R.drawable.list_mid_nor);
				layout_lostfound.setBackgroundResource(R.drawable.more_pressed);

				Intent intent = new Intent(MoreActivity.this,
						LostfoundActivity.class);
				startActivity(intent);

			}
		});

	}

}
