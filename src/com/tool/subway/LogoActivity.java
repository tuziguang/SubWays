package com.tool.subway;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;

public class LogoActivity extends Activity
{
	private LinearLayout layout_logo;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE); // 不显示标题
		setContentView(R.layout.activity_logo);

		layout_logo = (LinearLayout) findViewById(R.id.layoutLogo);
		AlphaAnimation animation = new AlphaAnimation(0.1f, 1.0f);
		animation.setDuration(5000);
		layout_logo.setAnimation(animation);
		animation.setAnimationListener(new Animation.AnimationListener()
		{

			@Override
			public void onAnimationStart(Animation animation)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationRepeat(Animation animation)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation)
			{
				// TODO Auto-generated method stub
				Intent intent = new Intent(LogoActivity.this,
						CDMainFaceActivity.class);
				startActivity(intent);
				finish();
			}
		});
	}


}
