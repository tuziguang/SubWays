package com.tool.subway;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class PiaowuguidingActivity extends Activity
{

	private WebView webviewPiaowu;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_PROGRESS);// 在标题栏顶部显示进度条
		setContentView(R.layout.activity_piaowuguiding);

		webviewPiaowu = (WebView) findViewById(R.id.Webview_piaowuguiding);

		if (webviewPiaowu != null)
		{
			webviewPiaowu.setWebViewClient(new WebViewClient()
			{
				@Override
				public void onPageFinished(WebView view, String url)
				{
					// TODO Auto-generated method stub
					dialog.dismiss();
					super.onPageFinished(view, url);
				}
			});
			
			// 加载成都地铁官方网站票务规定
			String path = "http://www.scqcp.com/";
			loadweb(path);
		}
		
		// 放大缩小的实现,手势多点触控
		webviewPiaowu.getSettings().setSupportZoom(true);
		webviewPiaowu.getSettings().setBuiltInZoomControls(true);
	}

	public void loadweb(String url)
	{
		if (webviewPiaowu!=null)
		{
			webviewPiaowu.loadUrl(url);
			dialog=ProgressDialog.show(PiaowuguidingActivity.this, null, "页面加载中，请稍后……");
			webviewPiaowu.reload();
		}
	}
}
